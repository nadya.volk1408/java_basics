package com.epam.rd.ryazan.at.java_basics.exercises;

import java.time.Month;
import java.time.format.TextStyle;
import java.util.Locale;

public class WorkWithNumbers {

	// task 1
	public static String toBinaryCode(int number) {
		return Integer.toBinaryString(number);
	}

	// task 2
	public static int fromHexCode(String hex) {
		return (int) Integer.parseInt(hex, 16);
	}

	// task 3
	public static String getMonthByNumber(int number) {
		if (number < 1 || number > 12) return null;
		return Month.of(number).getDisplayName(TextStyle.FULL, Locale.ENGLISH);
	}

	// task 4
	public static String roundByScale(double number, int scale) {
		return String.format("%." + scale + "f", number);
	}
}
