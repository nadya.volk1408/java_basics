package com.epam.rd.ryazan.at.java_basics.calculator;

import java.util.Scanner;
import java.util.Scanner;
import java.util.Scanner;
import java.util.List;

public class CalculatorMain {

	private static Scanner scanner;

	public static void main(String[] args) {
		scanner = new Scanner(System.in);
		calculation();
		scanner.close();
	}

	static String inputString() {
		return scanner.next();
	}

	static int InputNumber() {
		if (scanner.hasNextInt()) {
			return scanner.nextInt();
		} else {
			System.out.println("Error! Not integer value! Calculation will be incorrect!");
			return 0;
		}
	}

	static int InputNumber1() {
		if (scanner.hasNextInt()) {
			return scanner.nextInt();
		} else {
			System.out.println("Error! Not integer value! Calculation will be incorrect!");
			return 0;
		}
	}

	static int InputNumber2() {
		if (scanner.hasNextInt()) {
			return scanner.nextInt();
		} else {
			System.out.println("Error! Not integer value! Calculation will be incorrect!");
			return 0;
		}
	}

	private int value;

	static void calculation() {
		int number1;
		int number2;
		String sign;
		System.out.print("Enter the first number:");
		number1 = InputNumber();
		System.out.print("Enter the operation sign:");
		sign = inputString();
		System.out.print("Enter the second number:");
		number2 = InputNumber();
		switch (sign) {
			case "+":
				System.out.printf("Result = %d", Math.addExact(number1, number2));
				break;
			case "-":
				System.out.printf("Result = %d", Math.subtractExact(number1, number2));
				break;
			case "*":
				System.out.printf("Result = %d", Math.multiplyExact(number1, number2));
				break;
			case "/":
				if (number2 != 0) {
					System.out.printf("Result = %d", Math.floorDiv(number1, number2));
				} else {
					System.out.println("Error: divide by zero");
				}
				break;
			default:
				System.out.println("Error: incorrect operation");
		}
	}

}
