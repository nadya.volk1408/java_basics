package com.epam.rd.ryazan.at.java_basics.exercises;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class NumbersArray {

	// task 1
	public static int sumElementsOnEvenPositions(int[] array) {
		int S = 0;
		for (int i = 0; i < array.length; i = i + 2) {
			S += array[i];
		}
		return S;
	}

	// task 2
	public static void replaceNegativeElementsByZero(int[] array) {
		for (int i = 0; i < array.length; i++) {
	if (array[i] < 0) {
			array[i] = 0;
			     }
		}
	}

	// task 3
	public static int[] generateFibonacciNumbers(int count) {
		int[] result = new int[count];
		result[0] = 0;
		result[1]=1;
		for (int i = 2; i < count; i++) {
			result[i] = result[i - 1] + result[i - 2];
		}
		return result;
	}

	// task 4
	public static int[] generatePrimeNumbers(int count) {
		int[] RESULT = new int[count];
		int i = 0;
		int number = 2;
		loop:
		while (i < count) {
			for (int j = 2; j < number; j++) {
				if (number % j == 0) {
					number++;
					continue loop;
				}
			}
			RESULT[i++] = number++;
		}
		return RESULT;
	}

	// task 5
	public static int countNumbersWithEqualCountOfEvenAndOddDigits(int[] array) {
		int count = 0;
		for (int number : array) {
			int[] chars = Chars(number);
			int even = 0, odd = 0;
			for (int aChar : chars) {
				if (aChar % 2 == 0) {
					even++;
				} else {
					odd++;
				}
			}
			if (even == odd) count++;
		}
		return count;
	}

	// task 6
	public static int countNumbersWithOnlyUniqueDigits(int[] array) {
		int count = 0;
		loop:
		for (int number : array) {
			int[] chars = Chars(number);
			for (int j = 0; j < chars.length; j++) {
				for (int k = j + 1; k < chars.length; k++) {
					if (chars[j] == chars[k]) continue loop;
				}
			}
			count++;
		}
		return count;
	}

	private static int[] Chars(int number) {
		number = Math.abs(number);
		int[] result = new int[String.valueOf(number).length()];
		for (int i = result.length - 1; i >= 0; i--) {
			result[i] = number % 10;
			number /= 10;
		}
		return result;
	}

	// task 7
	public static int[][] rotateArray(int[][] array) {
		int x = array.length;
		int y = array[0].length;
		int[][] result = new int[y][x];
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				result[j][x - i - 1] = array[i][j];
			}
		}
		return result;
	}

	// task 8
	public static int[][] findDuplicates(int[] array) {
		List<Integer> collect = IntStream.of(array).boxed().collect(Collectors.toList());
		return collect.stream()
				.distinct()
				.map(i -> new int[]{i, Collections.frequency(collect, i)})
				.toArray(int[][]::new);
	}

}
