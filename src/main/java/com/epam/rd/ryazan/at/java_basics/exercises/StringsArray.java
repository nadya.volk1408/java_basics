package com.epam.rd.ryazan.at.java_basics.exercises;

import java.util.Arrays;

public class StringsArray {

	// task 1
	public static String findShortestString(String[] array) {
		String result = null;
		for (String string : array) {
			if (!string.isEmpty() && (result == null || string.length() < result.length())) {
				result = string;
			}
		}
		return result;
	}

	// task 2
	public static String[] findLongStrings(String[] array) {
		float sum = 0;
		for (String s : array) {
			sum += s.length();
		}
		sum /= array.length;
		String[] result = new String[array.length];
		int count = 0;
		for (String string : array) {
			if (string.length() > sum) {
				result[count++] = string + "=" + string.length();
			}
		}
		return Arrays.copyOf(result, count);
	}

	// task 3
	public static String findStringWithDigitsOnly(String[] row) {
		String digits = "[0-9]+";
		int index = 0;
		int count = 0;
		for (int i = 0; i < row.length; i++) {
			if (row[i].matches(digits)) {
				count++;
				index = i;
				if (count == 2) {
					break;
				}
			}
		}
		return count != 0 ? row[index] : null;
	}

}
