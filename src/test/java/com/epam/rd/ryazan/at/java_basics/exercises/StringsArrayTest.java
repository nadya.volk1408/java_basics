package com.epam.rd.ryazan.at.java_basics.exercises;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class StringsArrayTest {

	@Test
	public void testFindShortestString1() {
		String[] array = {"hello", "world", "22", ""};
		String result = StringsArray.findShortestString(array);
		assertEquals("22", result, "Result is incorrect for array: " + Arrays.toString(array));
	}

	@Test
	public void testFindShortestString2() {
		String[] array = {"", ""};
		String result = StringsArray.findShortestString(array);
		assertNull(result, "Result is incorrect for array: " + Arrays.toString(array));
	}

	@Test
	public void testFindLongStrings() {
		String[] array = {"hello", "world", "22", ""};
		String[] result = StringsArray.findLongStrings(array);
		String[] expected = {"hello=5", "world=5"};
		assertArrayEquals(expected, result, "Result is incorrect for array: " + Arrays.toString(array));
	}

	@Test
	public void testFindStringWithDigitsOnly1() {
		String[] array = {"hello", "world", "22", ""};
		String result = StringsArray.findStringWithDigitsOnly(array);
		assertEquals("22", result, "Result is incorrect for array: " + Arrays.toString(array));
	}

	@Test
	public void testFindStringWithDigitsOnly2() {
		String[] array = {"hello", "world", "22", "332"};
		String result = StringsArray.findStringWithDigitsOnly(array);
		assertEquals("332", result, "Result is incorrect for array: " + Arrays.toString(array));
	}

	@Test
	public void testFindStringWithDigitsOnly3() {
		String[] array = {"hello", "world", ""};
		String result = StringsArray.findStringWithDigitsOnly(array);
		assertNull(result, "Result is incorrect for array: " + Arrays.toString(array));
	}
}