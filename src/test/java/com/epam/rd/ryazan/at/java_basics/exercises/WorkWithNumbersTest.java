package com.epam.rd.ryazan.at.java_basics.exercises;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class WorkWithNumbersTest {

	@Test
	public void testToBinaryCode1() {
		int number = 11202;
		String result = WorkWithNumbers.toBinaryCode(number);
		assertEquals("10101111000010", result, "Result is incorrect for number: " + number);
	}

	@Test
	public void testToBinaryCode2() {
		int number = 0;
		String result = WorkWithNumbers.toBinaryCode(number);
		assertEquals("0", result, "Result is incorrect for number: " + number);
	}

	@Test
	public void testToHexCode() {
		String hex = "2BC2";
		int result = WorkWithNumbers.fromHexCode(hex);
		assertEquals(11202, result, "Result is incorrect for number: " + hex);
	}

	@Test
	public void testGetMonthByNumber1() {
		int number = 2;
		String result = WorkWithNumbers.getMonthByNumber(number);
		assertEquals("February", result, "Result is incorrect for number: " + number);
	}

	@Test
	public void testGetMonthByNumber2() {
		int number = -1;
		String result = WorkWithNumbers.getMonthByNumber(number);
		assertNull(result, "Result is incorrect for number: " + number);
	}

	@Test
	public void testRoundByScale() {
		double number = 1.6666666666666;
		String result = WorkWithNumbers.roundByScale(number, 2);
		assertEquals("1.67", result, "Result is incorrect for number: " + number);
	}
}