package com.epam.rd.ryazan.at.java_basics.calculator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CalculatorTest {

	private final InputStream systemIn = System.in;
	private final PrintStream systemOut = System.out;

	private ByteArrayInputStream testIn;
	private ByteArrayOutputStream testOut;

	@BeforeEach
	public void setUpOutput() {
		testOut = new ByteArrayOutputStream();
		System.setOut(new PrintStream(testOut));
	}

	private void provideInput(String... data) {
		String collect = String.join("\n", data);
		testIn = new ByteArrayInputStream(collect.getBytes());
		System.setIn(testIn);
	}

	private String getOutput() {
		return testOut.toString();
	}

	@AfterEach
	public void restoreSystemInputOutput() {
		System.setIn(systemIn);
		System.setOut(systemOut);
	}

	@Test
	public void whenTwoPlusThreeThenReturnsFive() {
		provideInput("2", "+", "3");
		CalculatorMain.main(new String[0]);
		String result = getOutput().split("Result\\s?=\\s?")[1];
		assertEquals("5", result);
	}

	@Test
	public void whenFiveMinusTenThenReturnsMinusFive() {
		provideInput("5", "-", "10");
		CalculatorMain.main(new String[0]);
		String result = getOutput().split("Result\\s?=\\s?")[1];
		assertEquals("-5", result);
	}

	@Test
	public void whenFourMultipliedByThreeThenReturnsTwelve() {
		provideInput("4", "*", "3");
		CalculatorMain.main(new String[0]);
		String result = getOutput().split("Result\\s?=\\s?")[1];
		assertEquals("12", result);
	}

	@Test
	public void whenSixDivideByMinusTwoThenReturnsMinusThree() {
		provideInput("6", "/", "-2");
		CalculatorMain.main(new String[0]);
		String result = getOutput().split("Result\\s?=\\s?")[1];
		assertEquals("-3", result);
	}

	@Test
	public void whenSixDivideByZeroThenReturnsError() {
		provideInput("6", "/", "0");
		CalculatorMain.main(new String[0]);
		assertTrue(getOutput().contains("Error: divide by zero"));
	}

	@Test
	public void whenIncorrectOperationThenReturnsError() {
		provideInput("6", "&", "2");
		CalculatorMain.main(new String[0]);
		assertTrue(getOutput().contains("Error"));
	}

	@Test
	public void whenIncorrectNumberThenReturnsError() {
		provideInput("6a", "&", "2");
		CalculatorMain.main(new String[0]);
		assertTrue(getOutput().contains("Error"));
	}
}