package com.epam.rd.ryazan.at.java_basics.exercises;


import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class NumbersArrayTest {

	@Test
	public void testSumElementsOnEvenPositions() {
		int[] array = {1, -2, 2, 3, 1, -5, -10};
		int result = NumbersArray.sumElementsOnEvenPositions(array);
		assertEquals(-6, result, "Result is incorrect for array: " + Arrays.toString(array));
	}

	@Test
	public void testReplaceNegativeElementsByZero() {
		int[] originalArray = {1, -2, 0, 3, 1, -5, -10};
		int[] array = Arrays.copyOf(originalArray, originalArray.length);
		NumbersArray.replaceNegativeElementsByZero(array);
		int[] expected = {1, 0, 0, 3, 1, 0, 0};
		assertArrayEquals(expected, array, "Result is incorrect for array: " + Arrays.toString(originalArray));
	}

	@Test
	public void testGenerateFibonacciNumbers() {
		int[] array = NumbersArray.generateFibonacciNumbers(14);
		int[] expected = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233};
		assertArrayEquals(expected, array, "Incorrectly generated Fibonacci numbers");
	}

	@Test
	public void testGeneratePrimeNumbers() {
		int[] array = NumbersArray.generatePrimeNumbers(10);
		int[] expected = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
		assertArrayEquals(expected, array, "Incorrectly generated prime numbers");
	}

	@Test
	public void testCountNumbersWithEqualCountOfEvenAndOddDigits() {
		int[] array = {11, 12, -22, -25, 145, 1588, 0};
		int result = NumbersArray.countNumbersWithEqualCountOfEvenAndOddDigits(array);
		assertEquals(3, result, "Result is incorrect for array: " + Arrays.toString(array));
	}

	@Test
	public void testCountNumbersWithOnlyUniqueDigits() {
		int[] array = {11, 12, -22, -252, 145, 1588, 484, -1907, 0};
		int result = NumbersArray.countNumbersWithOnlyUniqueDigits(array);
		assertEquals(4, result, "Result is incorrect for array: " + Arrays.toString(array));
	}

	@Test
	public void testRotateArray() {
		int[][] array = {
				{1, 2, 3},
				{4, 5, 6}
		};
		int[][] result = NumbersArray.rotateArray(array);
		int[][] expected = {
				{4, 1},
				{5, 2},
				{6, 3}
		};
		for (int i = 0; i < expected.length; i++) {
			assertArrayEquals(expected[i], result[i], "Result is incorrect for array row #" + i);
		}
	}

	@Test
	public void testFindDuplicates() {
		int[] array = {1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 2, -1, 2, 3, 43, 5, 5, 5, 1, 1, 1, 2, -1, -1};
		int[][] result = NumbersArray.findDuplicates(array);
		int[][] expected = {
				{1, 7},
				{2, 7},
				{3, 4},
				{-1, 3},
				{43, 1},
				{5, 3}
		};
		assertArrayEquals(expected, result, "Result is incorrect for array: " + Arrays.toString(array));
	}
}