## Java Basics

The code should be intelligently divided into methods. You may add specific methods for formatted output of results and
data generation.

You cannot use class java.util.Arrays in this module, Collections API and other libraries.

For tasks 1, 2 and 3 you should develop methods that resolve the task. You should also provide the code that demonstrate
the methods.

###### 1. Numbers array

Implement a set of methods that work with an integer numbers array. They should work with any amount of numbers in any
range.

1. Sum the elements on even positions.
1. Replace negative elements by zeros.
1. Generate X Fibonacci numbers (0, 1, 1, 2, 3, 5...).
1. Generate prime numbers ranging from 1 to X.
1. Count numbers with equal amount of even and odd digits.
1. Count number that consist only of different digits.
1. Having an array `int[X][Y]`, rotate it to `int[Y][X]` rotating the data clockwise.
1. Find the elements that occur more than once (not necessary in a row). Do not use sorting, do not change the original
   array. Present the result as an array `int[X][2]`, where Х is the count of repeating elements, first element in an
   array is the number itself, second is the number of repetitions.

###### 2. String array

Implement a set of methods that work with a Strings array.

1. Find the shortest String ignoring the empty ones (use method `String.length()`). If there are no such String, return
   `null`.
1. Find the Strings that are longer than the average length, and their length as well (present the result as an array
   of `string=6`).
1. Find a String containing only digits. If there are more than one such String, find the second one. If there is none,
   return `null`

###### 3. Work with numbers

1. Convert a positive number into a binary, return as a String.
1. Convert a number from a hexadecimal code to a number.
1. Find a month name using its number (1 - January). If the original number is out of range 1-12, return `null`.
1. Round the provided double number to X decimal places, return as String.

###### 4. Calculator

Write a calculator - a program that receives two integers as input (number A and number B), performs one arithmetic
operation (`+` - addition, `-` - subtraction,` * `- multiplication,` / `- division) and outputs the result to the
console as `Result = 4`. Provide a user menu to select the required operation. Catch the case of division by zero,
output the error `Error: divide by zero`. The input data should be entered from the keyboard using `java.util.Scanner`.